/// @file
/// @brief Section writer

#include "../include/pe_file_structure.h"
#include "../include/read_write_status.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Writes data specified by name from input file to output file
 * @param in Input file
 * @param out Output file
 * @param file PE file container
 * @param section_name Name of section to extract
 * @return write status
 */

enum write_status extract_section_to_file(FILE *in, FILE *out, struct PEFile *file, char* section_name) {
    for (size_t section_iterator=0; section_iterator<file->header.number_of_sections; section_iterator++){
        if (strcmp((char*) file->section_headers[section_iterator].name, section_name)==0){
            fseek(in, file->section_headers[section_iterator].pointer_to_raw_data, SEEK_SET);
            char* data = malloc(file->section_headers[section_iterator].size_of_raw_data);
            fread(data, file->section_headers[section_iterator].size_of_raw_data, 1, in);
            fwrite(data, file->section_headers[section_iterator].size_of_raw_data, 1, out);
            free(data);
            return WRITE_OK;
        }
    }
    return ERROR_SECTION_NOT_FOUND;
}
