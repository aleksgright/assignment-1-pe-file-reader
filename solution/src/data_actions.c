/// @file
/// @brief Data actions

#include "../include/data_actions.h"

/**
 * @brief Converts 4 byte word from little endian to big endian and vice versa
 * @param val 4 byte word
 * @return 4 byte word in another endian
 */

uint32_t little_to_big_endian(const uint32_t val) {
    return ((val & 0xff000000) >> 24) |
           ((val & 0x00ff0000) >> 8) |
           ((val & 0x0000ff00) << 8) |
           ((val & 0x000000ff) << 24);
}
