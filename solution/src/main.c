/// @file 
/// @brief Main application file

#include "../include/pe_file_structure.h"
#include "../include/read_write_status.h"

#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage example
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f) {
    fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char **argv) {
    if (argc != 4) {
        usage(stdout);
        return 1;
    }

    FILE *source_file = fopen(argv[1], "rb");
    if (source_file == NULL) {
        fprintf(stderr, "Failed to open file");
        return 1;
    }

    FILE *target_file = fopen(argv[3], "wb ");
    if (source_file == NULL) {
        fprintf(stderr, "Failed to open output file");
        return 1;
    }

    struct PEFile *file = malloc(sizeof(struct PEFile));
    if (read_pe_file(source_file, file)) {
        printf("Error while reading file");
        free(file);
        return 1;
    }

    if (extract_section_to_file(source_file, target_file, file, argv[2])) {
        free(file->section_headers);
        free(file);
        fclose(source_file);
        fclose(target_file);
        printf("Error while writing data");
        return 1;
    }
    free(file->section_headers);
    free(file);
    fclose(source_file);
    fclose(target_file);
    return 0;
}
