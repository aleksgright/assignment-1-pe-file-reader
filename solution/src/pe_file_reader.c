/// @file
/// @brief PE file reader

#include "../include/data_actions.h"
#include "../include/pe_file_structure.h"
#include "../include/read_write_status.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Reading header and section table from PE file
 * @param in Input file
 * @param file PE file container
 * @return read status
 */

enum read_status read_pe_file(FILE *in, struct PEFile *file) {
    fseek(in, POINTER_TO_HEADER_ADDRESS, SEEK_SET);
    fread(&file->header_offset, sizeof(uint32_t), 1, in); //seeking pointer to header
    fseek(in, file->header_offset, SEEK_SET);
    fread(&file->magic, sizeof(file->magic), 1, in); // reading signature
    if (little_to_big_endian(file->magic) != PE_SIGNATURE) {
        return ERROR_INVALID_FORMAT;
    }
    fread(&file->header, sizeof(file->header), 1, in); // reading header
    fseek(in, file->header.size_of_optional_header, SEEK_CUR); //skipping optional header
    file->section_headers = malloc(sizeof(struct SectionHeader) * file->header.number_of_sections);
    if (fread(file->section_headers, sizeof(struct SectionHeader), file->header.number_of_sections, in) !=
        file->header.number_of_sections) {
        free(file->section_headers);
        return ERROR_READING_SECTION_TABLE;
    }
    return READ_OK;
}
