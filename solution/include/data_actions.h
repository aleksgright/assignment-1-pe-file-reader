#ifndef DATA_CONTAINER_H
#define DATA_CONTAINER_H

#include <stdint.h>
#include <stdlib.h>

uint32_t little_to_big_endian(const uint32_t val);
#endif
