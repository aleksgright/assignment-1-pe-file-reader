#ifndef READ_WRITE_STATUS_H
#define READ_WRITE_STATUS_H

#include <stdio.h>

/**
 * brief Status codes for reading from file
 */

enum read_status {
    READ_OK = 0,
    ERROR_INVALID_FORMAT,
    ERROR_READING,
    ERROR_READING_HEADER,
    ERROR_READING_SECTION_TABLE
};

/**
 * brief Status codes for writing to file
 */

enum write_status {
    WRITE_OK = 0,
    ERROR_WRITING,
    ERROR_SECTION_NOT_FOUND
};

enum read_status read_pe_file(FILE *in, struct PEFile *file);

enum write_status extract_section_to_file(FILE *in, FILE *out, struct PEFile *file, char *section_name);

#endif
