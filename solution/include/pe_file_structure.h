#ifndef PE_FILE_STRUCTURE
#define PE_FILE_STRUCTURE

#include <stdint.h>

///Max length of a section name
#define MAX_SECTION_NAME_LEN 8
///PE file magic
#define PE_SIGNATURE 0x50450000
///Location of pointer to PE header
#define POINTER_TO_HEADER_ADDRESS 0x3C

/**
 * Structure of PE header
 */

#if defined(_MSC_VER)
#pragma pack(push, 1)
#endif
struct PEHeader {
    ///The number that identifies the type of target machine
    int16_t machine;
    ///The number of sections
    int16_t number_of_sections;
    ///The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created
    int32_t time_date_stamp;
    ///The file offset of the COFF symbol table, or zero if no COFF symbol table is present
    int32_t pointer_to_symbol_table;
    ///The number of entries in the symbol table
    int32_t number_of_symbols;
    ///The size of the optional header, which is required for executable files but not for object files
    int16_t size_of_optional_header;
    ///The flags that indicate the attributes of the file
    int16_t characteristics;

}
#if defined(__clang__) || defined(__GNUC__)
    __attribute__((packed));
#elif  defined(_MSC_VER)
;#pragma pack(pop)
#endif

/**
 * Structure of section header
 */

#if defined(_MSC_VER)
#pragma pack(push, 1)
#endif
struct SectionHeader {
    ///An 8-byte, null-padded UTF-8 encoded string.
    uint8_t name[MAX_SECTION_NAME_LEN];
    ///Total size of the section when it's loaded in memory
    union {
        ///Physical size of th section
        uint32_t physical_size;
        ///Virtual size of the section
        uint32_t virtual_size;
    } Misc;
    ///For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
    uint32_t virtual_address;
    ///The size of the section or the size of the initialized data on disk.
    uint32_t size_of_raw_data;
    ///The file pointer to the first page of the section within the COFF file.
    uint32_t pointer_to_raw_data;
    ///The file pointer to the beginning of relocation entries for the section.
    uint32_t pointer_to_relocations;
    ///The file pointer to the beginning of line-number entries for the section.
    uint16_t pointer_to_linenumbers;
    ///The number of relocation entries for the section.
    uint32_t number_of_relocations;
    ///The number of line-number entries for the section.
    uint16_t number_of_linenumbers;
    ///The number of line-number entries for the section.
    uint32_t characteristics;
}
#if defined(__clang__) || defined(__GNUC__)
    __attribute__((packed));
#elif  defined(_MSC_VER)
;#pragma pack(pop)
#endif


struct PEFile {
    /// @name Offsets within file
    ///@{
    /// Offset to a main PE header
    uint32_t header_offset;
    ///@}

    /// @name File headers
    ///@{
    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;
    ///@}
};
#endif
